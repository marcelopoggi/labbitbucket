using System;
using System.Windows.Forms;

namespace LPTeste
{
  //============================================================================================
  // Projeto      LPTeste 
  // Versão       Documento: 1.0.0.0
  // Criado:      19/01/2020 - 21:15:18 - Domingo - Poggi 
  // Modificado:  19/01/2020 - 21:15:18 - Domingo - Poggi 
  // Tabela:      
  // Arquivo:     Program.cs
  // Versão:      
  //============================================================================================ 
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Properties.Settings s = new Properties.Settings();

            ConfiguracaoInicialTO ci = new ConfiguracaoInicialTO
            {
                TituloAplicativo = "LPTeste",
                CaminhoPadrao = Application.StartupPath,
                //CaminhoMontagem = s.CaminhoMontagem,
                CaminhoMontagem = @"D:\_Temporario\LPTeste",
                //CaminhoBackup = s.CaminhoBackup
            };

            Principal ini = new Principal(ci);
        }
    }
}
